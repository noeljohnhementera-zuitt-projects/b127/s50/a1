// Bootstrap
import { Jumbotron, Button, Row, Col } from 'react-bootstrap';

export default function Banner() {
	return (
	<Row>
		<Col className="mt-3">
			<Jumbotron>
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Opportunities for everyone, everywhere.</p>
				<Row>
					<Col>
						<Button variant="primary">Enroll Now!</Button>
					</Col>
					<Col>
						<Button variant="outline-primary" size="lg">Enroll Now!</Button>
					</Col>
				</Row>
			</Jumbotron>
		</Col>
	</Row>
		)
}