import { useState, useEffect, Fragment } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminView(props){

	const { coursesData, fetchData } = props
	// Dito nag-destructure para magamit ang props
	console.log(props)
	const [courses, setCourses] = useState([])

	//Add state for addCourse
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	//States to our modals if it needs to open/close
	const [showAdd, setShowAdd] = useState(false);
	//Functions to handle opening and closing our Add New Course Modal
	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);

	// States for our edtCourse modals
	const [ showEdit, setShowEdit ] = useState(false);

	// Add a state for courseId for the fetch URL
	const [ courseId, setCourseId ] = useState('');

	//Functions to handle opening and closing in our Edit Course Modal. We need to reset all relevant states back to their default ones.
	const closeEdit = () =>{
		setShowEdit(false)
		setName('')
		setDescription('')
		setPrice(0)
	}

	const openEdit = (courseId) =>{
		fetch(`https://secure-sea-51149.herokuapp.com/courses/${ courseId }`)
		.then(res => res.json())
		.then(data => {
			// Populate all input values with the course information that we fetched
			setCourseId(data._id)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
		// Then, open the modal
		setShowEdit(true)
	}

	useEffect(()=>{
		const coursesArr = coursesData.map(course => {
			return(
				<tr key={course._id}> {/*the purpose is to keep on track of the data. Para maiwasan ang error*/}
					<td>{course._id}</td>
					<td>{course.name}</td>
					<td>{course.description}</td>
					<td>{course.price}</td>
					<td className={course.isActive ? "text-success" : "text-danger"}>
						{course.isActive ? "Available" : "Unavailable" }
					</td>
					<td>
						<Button variant="primary" size="sm" onClick={() => openEdit(course._id)}>Update</Button>
						{course.isActive ? 
						<Button variant='danger' size='sm' onClick={() => archiveToggle(course._id, course.isActive)}>Disable</Button>
						:
						<Button variant='success' size='sm' onClick={() => activateToggle(course._id, course.isActive)}>Enable</Button>
						}
					</td>
				</tr>

				)
		})
		setCourses(coursesArr)
	}, [coursesData])

	// Add A Course function
	const addCourse = (e) =>{
		e.preventDefault();
		fetch('https://secure-sea-51149.herokuapp.com/courses/', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken') }`
				// dahil may auth na sinet sa back-end natin
				// makikita ang authorization sa Inspect > Application
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			// check Inspect > Network if ano ang status
			if(data === true){
				// Run our fetchData function that we passed from our parent component, in order to re-render our page.
				fetchData()
				/* automatic nagrerender after adding a new course
				   alternative
						window.location.reload()*/

				Swal.fire({
					title: "Success!",
					icon: 'success',
					text: 'Course successfully added!'
				})

				setName('')
				setDescription('')
				setPrice(0)

				//automatic close the modal
				closeAdd()
			}else{
				Swal.fire({
					title: "Something went wrong!",
					icon: 'error',
					text: 'Please try again!'
				})
			}
		})
	}

	// Edit Course function
	const editCourse = (e, courseId) =>{
		e.preventDefault(); // prevents the default behavior
		fetch(`https://secure-sea-51149.herokuapp.com/courses/${ courseId }`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify ({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true) {
				fetchData()
				Swal.fire({
					title: "Success!",
					icon: 'success',
					text: 'Course successfully updated!'
				})
				closeEdit()
			}else{
				fetchData()
				Swal.fire({
					title: "Something went wrong!",
					icon: 'error',
					text: 'Please try again!'
				})
			}
		})
	}

	// Archive / Disable a course
	const archiveToggle = (courseId, isActive) =>{
		fetch (`http://localhost:4000/courses/${ courseId }/archive`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify ({
				isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data =>{
			if(data === true) {
				fetchData()
				Swal.fire({
					title: "Success!",
					icon: 'success',
					text: 'Course successfully disabled!'
				})
			}else{
				fetchData()
				Swal.fire({
					title: "Something went wrong!",
					icon: 'error',
					text: 'Please try again!'
				})
			}
		})
	}

	// Activate / Enable Button
	const activateToggle = (courseId, isActive) =>{
		fetch (`http://localhost:4000/courses/${ courseId }/activate`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify ({
				isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data =>{
			if(data === true) {
				fetchData()
				Swal.fire({
					title: "Success!",
					icon: 'success',
					text: 'Course successfully activated!'
				})
			}else{
				fetchData()
				Swal.fire({
					title: "Something went wrong!",
					icon: 'error',
					text: 'Please try again!'
				})
			}
		})
	}

	return(
		<Fragment>
			<div className="text-center my-4">
				<h2>Admin Dashboard</h2>
				<div className="d-flex justify-content-center">
					<Button variant="primary" onClick={openAdd}> Add New Course </Button>
				</div>
			</div>
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{courses}
				</tbody>
			</Table>

		{/*ADD NEW COURSE MODAL*/}

			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addCourse(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Course</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Name:</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required />
						</Form.Group>

						<Form.Group>
							<Form.Label>Description:</Form.Label>
							<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required />
						</Form.Group>

						<Form.Group>
							<Form.Label>Price:</Form.Label>
							<Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required />
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

		{/*Edit Course Modal*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editCourse(e, courseId)}>
					<Modal.Header closeButton>
						<Modal.Title>Update Course</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Name:</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required />
						</Form.Group>

						<Form.Group>
							<Form.Label>Description:</Form.Label>
							<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required />
						</Form.Group>

						<Form.Group>
							<Form.Label>Price:</Form.Label>
							<Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required />
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		</Fragment>
		)
}



// import { useState, useEffect, Fragment } from 'react';
// import { Table, Button, Modal, Form } from 'react-bootstrap';

// export default function AdminView (props) {
	
//	const { coursesData } = props
//	console.log(props)

//	const [ courses, setCourses ]  = useState([])

	// Add useState() for adding a course
//	const [name, setName] = useState('');
//	const [description, setDescription] = useState('');
//	const [price, setPrice] = useState(0);

	// States to our modals if it needs to open or close
//	const [showAdd, setShowAdd] = useState(false);
	// Functions to handle opening and closing out Add New Course Button Modal
//	const openAdd = () => setShowAdd(true);
//	const closeAdd = () => setShowAdd(false);

//	useEffect(()=>{
//		const courseArr = coursesData.map(course =>{
//			return (
//				<tr key={ course._id}>
//					<td>{ course._id}</td>
//					<td>{ course.name}</td>
//					<td>{ course.description}</td>
//					<td>{ course.price}</td>
//					<td className={course.isActive ? "text-success" : "text-danger"}>
//						{ course.isActive ? "Available" : "Unavailable"}</td> {/*// Ternary operator kapag //hindi string*/}
//				</tr>
//			)
//		})
//		setCourses(courseArr)
//	}, [coursesData]) 	

//	return (
//	<Fragment>
//		<div className='text-center my-4'>
//			<h2>Admin Dashboard</h2>
//			<div className='d-flex justify-content-center'> {/*Magkasama silang dalawa*/}
//				<Button variant='primary' onclick={openAdd}>Add New Course</Button>
//			</div>
//		</div>
//
//		<Table striped bordered hover responsive>
//			<thead className='bg-dark text-white'>
//				<tr>
//					<th>ID</th>
//					<th>Name</th>
//					<th>Description</th>
//					<th>Price</th>
//					<th>Availability</th>
//					<th>Actions</th>
//				</tr>
//			</thead>
//			<tbody>
//				{courses} {/*const [ courses, setCourses ]  = useState([])*/}
//			</tbody>
//		</Table>
//	{/*Add New Course Modal*/}
//		<Modal show={showAdd} onHide={closeAdd}>
//			<Form>
//				<Modal.Header closeButton>
//					<Modal.Title>Add Course</Modal.Title>
//				</Modal.Header>
//				<Modal.Body>
//					<Form.Group>
//						<Form.Label>Name:</Form.Label>
//						<Form.Control 
//							type='text'
//							value={name}
//							onChange={e => setName(e.target.value)}
//							required
//						/>
//					</Form.Group>
//					<Form.Group>
//						<Form.Label>Description:</Form.Label>
//						<Form.Control 
//							type='text'
//							value={description}
//							onChange={e => setDescription(e.target.value)}
//							required
//						/>
//					</Form.Group>
//					<Form.Group>
//						<Form.Label>Price:</Form.Label>
//						<Form.Control 
//							type='text'
//							value={price}
//							onChange={e => setPrice(e.target.value)}
//							required
//						/>
//					</Form.Group>
//				</Modal.Body>
//
//				<Modal.Footer>
//					<Button variant="secondary" onClick={closeAdd}>Close</Button>
//					<Button variant="success" type="submit">Submit</Button>
//				</Modal.Footer>
//			</Form>
//		</Modal>
//	</Fragment>
//	)
// }

