
/*
Separation of concerns
	* Put this in a separate folder. AppNavbar.js is a component
*/

/*function AppNavBar(){
	return(
		)
}
export default AppNavBar;	// or
*/

// Import in Bootstrap
import {Navbar, Nav} from 'react-bootstrap';

// s47 Router
import { Link, NavLink } from 'react-router-dom';
import { Fragment, useContext } from 'react'; 
// useContext - to untack ang laman ng UserContext user, setUser ang value

// React Context
import UserContext from '../UserContext';

// We use Pascal, capital letter first letter 
	// = para hindi mag-overlap sa HTML tags na ginagamit
	// = para makuha ang React syntax
// Kailngan may forward slash para hindi mag-error
// className = class but it is used in React
	// className is similar to class sa HTML / CSS
// <Navbar.Toggle> and <Navbar.Collapse> is used to have a hamburger icon

// Function based / components easy to create a task sa function unlike the class based
export default function AppNavBar(){
	
	const { user } = useContext(UserContext) // we untacked the value of UserContext wc is user

	//Set a conditional rendering <UserContext.Provider> sa App.js
		// Kung ano ang gusto lang natin na ipakita
	// need to import useContext para mauntack ang value sa UserContext.js wc is user, setUser
	let rightNav = (user.accessToken !== null) ?
		<Fragment>													{/*If mayg nabasang nakasave na email*/}
			<Nav.Link as={NavLink} to="/logout"> Logout </Nav.Link>
		</Fragment>
		:
		<Fragment>
			<Nav.Link as={NavLink} to="/login">Login</Nav.Link>		{/*If walanag nabasang nakasave na email*/}
			<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
		</Fragment>
	return(
			<Navbar bg="light" expand="lg"> 
				<Navbar.Brand as={Link} to="/">Zuitt Booking System</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" /> 
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ml-auto"> 
						<Nav.Link as={NavLink} to="/">Home</Nav.Link>
						<Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
						{/*<Nav.Link as={NavLink} to="/login">Login</Nav.Link>*/}
						{/*<Nav.Link as={NavLink} to="/register">Register</Nav.Link>*/}
					</Nav>
					<Nav className="ml-auto">
						{rightNav}
					</Nav>
				</Navbar.Collapse>
			</Navbar>
		)
}

/*
// Old way - Using Class Based
import { Component } from 'react';

export default class AppNavBar extends Component(){
	return(
			<Navbar bg="light" expand="lg"> 
				<Navbar.Brand href="#home">Zuitt Booking System</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" /> 
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ml-auto"> 
						<Nav.Link href="#home">Home</Nav.Link>
						<Nav.Link href="#courses">Courses</Nav.Link>
					</Nav>
				</Navbar.Collapse>
			</Navbar>
		)
}
*/