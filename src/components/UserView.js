import { useState, useEffect, Fragment } from 'react';
import CourseCard from './CourseCard';

export default function UserView({ coursesData }) {

	const [courses, setCourses] = useState([])
	console.log(coursesData)

	useEffect(()=>{

		const courseArr = coursesData.map(course => {
			// only render active courses
			if(course.isActive === true){
				return (
					<CourseCard courseProp={course} key={course._id}/>
				)				// courseProp is the props from CourseCard component
			}else{
				return null;
			}
		})

		// set the courses state to the result of our map function, to bring our returned course components outside of the scope of our useEffect where our return statement below can see it
		setCourses(courseArr)

	}, [coursesData]) // ang laman is states
						// useEffect() kung ano ang data na marereceive sa parent component(Courses), magrender agad sya

	return (
		<Fragment>
			{courses}
		</Fragment>
	)
}