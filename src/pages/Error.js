import { Link } from 'react-router-dom';

import { Jumbotron, Container } from 'react-bootstrap';

export default function Error () {
	return (
	<Container className='mt-5'>
		<Jumbotron>
			<h1>Page Not Found! | Error 404</h1>
			<p>Go back to the <Link to="/">Homepage</Link></p>
		</Jumbotron>
	</Container>
	)
}