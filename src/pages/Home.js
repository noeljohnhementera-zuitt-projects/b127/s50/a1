
// This is the Parent Component
	// Dahil ito ang nagpapasa ng data papunta sa Child Component

// If multiple components ang gagamitin
import { Fragment } from 'react';

import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
/*import CourseCard from '../components/CourseCard';*/

/*import Welcome from '../components/Welcome';*/

export default function Home () {
	return (
		<Fragment>
			{/*< Welcome name="Jane" age={25}/>*/}
			< Banner />
			< Highlights />
			{/*< CourseCard />*/}
		</Fragment>
	)
}

// Use { } if hindi string ang value eg age: {25}