// useEffect() -> for validation, fetching
// useState() -> stop, wait, and go. >ay default and pang-update na data
// useContext -> hook na ginagamit para ma-unwrap ang laman sa reactContext (UserContext)
				// para mabuksan laman ng UserContext (React.createContext -> dito na-store yung data)		
// reactContext -> Global state where we can pass the data. folder kung saan ilalagay yung UserContext eg. { user, setUser, unsetUser}
				// Global useContext() si App.js
// props -> nagpapasa ng data whether object or states from parent to child


/*import courseData from '../data/courseData';
import CourseCard from '../components/CourseCard';
import { Fragment } from 'react';

export default function Courses (){
	// Checks to see if the mock data was captured
	console.log(courseData)
	console.log(courseData[0])

	// for us to be able to display the courses from the data file, we are going to use the map()
	// The "map" method loops through the individual course objects in our array and returns a component for each course
	// Multiple components created through the map method must have a UNIQUE KEY that will help Reactjs identify which elements
		// have been changed, added or removed
	// Laging may kasama na key={course.id} = to keep track of the data of courses
	
	// Props kinuha sa CourseCard
	const courses = courseData.map(course =>{ // course is a props
		return (

			< CourseCard key = {course.id} courseProp={course}/>)
	})

	// Looping ang courseData database, ilalabas lahat ang laman ng course data using map()
	// We'll use this if we want to get all of the data sa database natin (CourseData)

	return (
		<Fragment>
		<h1>Courses</h1>
		{< CourseCard courseProp={courseData[0]} />} {/*If one item lang ang nasa card*///}
		
//		{courses}
//		</Fragment>
//	)
//}


import { useState, useEffect, useContext } from 'react';
//Bootstrap
import { Container } from 'react-bootstrap';
// components para sa sinet upon logging in
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';
// React context
import UserContext from '../UserContext';
export default function Courses() {

	const { user } = useContext(UserContext);

	const [allCourses, setAllCourses] = useState([]) // lahat ng makukuha kay fetch mapupunta dito

	const fetchData = () =>{
		fetch('https://secure-sea-51149.herokuapp.com/courses/all')
		// Automatic GET method if not additional options
		.then(res => res.json())
		.then(data =>{
			console.log(data)
			setAllCourses(data)
		})
	}

	useEffect(()=>{ 	// kailangan ito for initial rendering and para mareuse ang fetchData() function
		fetchData()		// para mafetch ang data(courses) sa function
	}, []) // initial/single rendering kapag blank. Hindi siya mag-uupdate
			// hiniwalay para ma-reuse ang fetchData()
			// para hindi na mag-hard refresh

	return (
		// ternary operator, laging nasa curly braces
		<Container>
			{
				(user.isAdmin === true) ? 
				// <AdminView coursesData={allCourses}/>
				<AdminView coursesData={allCourses} fetchData={fetchData}/> // this is used to render our page
				: 
				<UserView coursesData={allCourses}/>
			}
		</Container>
	)
}

// Kung sino ang nagbibigay ng data, siya ang parent component 
	// Parent si Courses
	// Child si AdminView at UserView