import { useContext, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';




export default function Logout () {

	// When a user logs out, buburahin ang email
	const { setUser, unsetUser } = useContext(UserContext) // value na kinuha sa UserContext in App.js

	// Clear the localStorage of the user's information
	unsetUser();

	// Placing the 'setUser' setter function inside of a userEffect is necessary because of updates within React.js that a state of another component cannot be updated while trying to render a different component
	// By adding the useEffect, this will allow the logout page to render first before trigerring the useEffect which changes the state of the user

	useEffect(()=>{
		// Set the user state back to its original value
		setUser({ accessToken: null })
	}, []) // array with value. sya ang responsible sa pag-update ng setUser
				// if walang value, initial rendering or once lang mag-update
		   // the array in our useEffect provide initial rendering onl;y

	return (
		// Redirect back to login
		< Redirect to='/login' />
	)
}