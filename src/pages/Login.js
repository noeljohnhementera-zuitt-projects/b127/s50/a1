// Unang ginawa to connect front to back
// useEffect() has is re-rendering / to update unlike the normal function will only follow the statement

import { Fragment, useState, useEffect, useContext } from 'react';
// useContext data that we can pass all throughout our components. It sets as global
// ginagamit ito sa states
// Kapag props, parent to child lang while useContext react-context is global
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

//React Context
// The purpose of importing is to store data into string
import UserContext from '../UserContext'

// s48 Routing
import { Redirect, useHistory } from 'react-router-dom';

export default function Login () {
	// The useHistory() hook gives you access to the history instance that you may use to navigate to access the location
	const history = useHistory();

	// useContext is a React hook used to unwrap our context. It will return the data passed as values by a provider(UserContext.Provider component in App.js)
	const { user, setUser } = useContext(UserContext)

	// useState() to Input Fields
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('') 

	// useState() Login Button
	const [isAuthenticated, setIsAuthenticated] = useState(false)

	// useEffect() to enable Login Button
	// useEffect() will re-rendering / to update the set useState() hook
	useEffect(() =>{
		if(email !== '' && password !== ''){
			setIsAuthenticated(true)
		}else{
			setIsAuthenticated(false)
		}
	}, [email, password])

	// Login alert and clear characters in the input field
	const clearInputData = (e) =>{
		e.preventDefault();

		// fetch using our backend server
			// it has 2 arguments
				// 1 URL from the server (routes)
				// 2. Optional object which contains additional information about our requests such as method, headers, body, etc
		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({ // to convert to a string
				email: email, // useState() getter / initial kinuha
				password: password // this are properties na nakaset sa back-end
				// first email: -> property at the back-end
				// second email -> getter of useState
			})
		})	
		.then(res => res.json())
		.then(data =>{
			console.log(data) // To check the access token
			if(data.accessToken !== undefined){
				localStorage.setItem('accessToken', data.accessToken)
				setUser({accessToken: data.accessToken});

				Swal.fire({
					title: 'Yay!',
					icon: 'success',
					text: 'Thank you for logging in to Zuitt Booking System'
				})

				// Get user's details from our token kapag successfully nakuha ang accessToken
				fetch('https://secure-sea-51149.herokuapp.com/users/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}` // base sa accessToken ni user na nag-login kukunin ang detailss
					}												// nakaconnect na ang access token kaya ito ang nilagay
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)

					if(data.isAdmin === true){
						localStorage.setItem('email', data.email)
						localStorage.setItem('isAdmin', data.isAdmin)
						setUser({
							email: data.email,
							isAdmin: data.isAdmin
						})
						// If admin, redirect the page to /courses
						// push(path / endpoint) - pushes a new entry onto the history stack
						history.push('/courses')
					}else{
						// If not admin, redirect to Homepage
						// walang masasave na email at isAdmin
						history.push('/')
					}
				})

			}else{
				Swal.fire({
					title: 'oops!',
					icon: 'error',
					text: 'Something Went Wrong. Check Your Credentials!'
				})
			}

			setEmail('')
			setPassword('')
		})

		/*setEmail('')
		setPassword('')

		Swal.fire({
			title: 'Authentication Successful!',
			icon: 'success',
			text: 'You have successfully logged in!'
		})
		Previous dicussion
		*/

		// React Context
		// local storage allows us to save data within our browser as strings
		// The setItem() method of the Storage Interface, when passed a key name and value, will add that key to the given storage ocjext, or update the key's value if it already exists
		// setItem is used to store data in the localStorage as a string
		// setItem('key', value)

		/*localStorage.setItem('email', email);
		setUser ( {email: email} )
		// This will be stored sa Application when Inspect with a key of email*/

		// once mag-login si user, madadagdag sya sa localStorage data
		// makukuha na natin ang laman sa useState sa App.js
			// const [user, setUser] = useState({email: localStorage.getItem('email')})
	}

	// Create a conditional statement that will redirect the user to the homepage when the user is logged in.
	return (
			(user.accessToken !== null) ?
				<Redirect to = "/" />	// After logging in, user will redirect to the homepage
				:
		<Fragment>
			<h1>Login</h1>
			<Form onSubmit={(e)=> clearInputData(e)}>
				<Form.Group>
					<Form.Label>Email Address:</Form.Label>
					<Form.Control
						type="email"
						value={email}
						onChange={e => setEmail(e.target.value)}
						placeholder="Enter Your Email Address"
						required
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password:</Form.Label>
					<Form.Control
						type="password"
						value={password}
						onChange={e => setPassword(e.target.value)}
						placeholder="Enter Your Password"
						required
					/>
				</Form.Group>
				{isAuthenticated ?
				<Button variant="success" type="submit">Login</Button>
								 :
				<Button variant="danger" type="submit" disabled>Login</Button>
				}
			</Form>
		</Fragment>
	)
}