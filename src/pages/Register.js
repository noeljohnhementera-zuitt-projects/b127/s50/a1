import { Fragment, useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
// Import stylish alert
import Swal from 'sweetalert2';

// s48 minbi-activity
import { Redirect, useHistory } from 'react-router-dom';
import UserContext from '../UserContext';
// same as <input> sa HTML ang <Form.Control>	
export default function Register () {

	// create useHistory() variable
	const history = useHistory();

	// useContext to get user(getter) in UserContext.js
	const { user } = useContext(UserContext)
	
	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	// Set other informations based on the server
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	// Hindi tayo makakapagtype using this variables because of the useState / initial value

	// gagamitan ng useState() sa Button
	const [isActive, setIsActive] = useState(false);

	// Check if the value are successfully not binded or changed
	console.log(email)
	console.log(password1)
	console.log(password2)
	console.log(firstName)
	console.log(lastName)
	console.log(mobileNo)

	// useEffect() for button fo rendering purposes
	useEffect(()=>{
		// Validation to enable submit button whwn all fields are populated and both passwords match
		if((email !== '' && password1 !== '' && password2 !== ' ' && firstName !== '' && lastName !== '' && mobileNo.length === 11) 
			&& (password1 === password2)){ // verification statement
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	}, [email, password1, password2, firstName, lastName, mobileNo]) // getter / initial value ang ilalagay sa array
										// controls the arrow function
										// for rendering purposes only lang. walang mababago
	
	// for the alert and after data submission
	function registerUser(e){ // e or parameter e is required when making an event listener / virtual DOM
		e.preventDefault();

		fetch('https://secure-sea-51149.herokuapp.com/users/checkEmail', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify ({
					email: email
				})
			})
				.then(res => res.json())
				.then(data =>{
					console.log(data)
					if(data === true){
						Swal.fire({
						title: 'Oops!',
						icon: 'error',
						text: 'Email already exists!',
						})
					}else{
						fetch('https://secure-sea-51149.herokuapp.com/users/register', {
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						},
						body: JSON.stringify ({
							firstName: firstName,
							lastName: lastName,
							mobileNo: mobileNo,
							email: email,
							password: password1
						})
					})
						.then(res => res.json())
						.then(data =>{
							console.log(data)
							if(data === true){
								Swal.fire({
									title: "Success!",
									icon: "success",
									text: "You have successfully registered!"
								})
									history.push("/login")
							}else{
								Swal.fire({
									title: "Oops!",
									icon: "error",
									text: "Please check your inputs!"
								})
							}
						})
					}
				})

		setEmail('')
		setPassword1('')
		setPassword2('') // this will remove all the input data after hitting the alert
		/*alert('Thank you for registering!')*/
		/*Swal.fire('Thank you for registering!')*/
		setFirstName('')
		setLastName('')
		setMobileNo('')
		/*Swal.fire({
			title: 'YaaaAAaaAAAaaayyYYyy huehue!',
			icon: 'success',
			text: 'You have successfully registered!'
		})*/
	}

	// Two-way binding
		// The values in the fields of the form is bound to the getter of the state and the event is bound to the setter.
		// This is called two way binding
			// The data we change in the view has updated the state
			// The data in the state has updated the view

	return (
			(user.accessToken !== null) ?
				<Redirect to = '/' />
			    :
		<Fragment>
			<h1>Register</h1>
			<Form onSubmit={(e) => registerUser(e)}> {/*para hindi magblank ang input fields upon submit button*/}
				<Form.Group>
					<Form.Label>First Name:</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter your First Name"
						value={firstName}
						onChange={e => setFirstName(e.target.value)}
						required
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Last Name:</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter your Last Name"
						value={lastName}
						onChange={e => setLastName(e.target.value)}
						required
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Mobile Number:</Form.Label>
					<Form.Control
						type="text"
						maxlength="11"
						placeholder="Enter your Mobile Number"
						value={mobileNo}
						onChange={e => setMobileNo(e.target.value)}
						required
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Email Address:</Form.Label>
					<Form.Control 
						type="email"
						placeholder="Enter your Email"
						value={email} // I can't type dahil "" ang initial value. I need an onChange() event.
						onChange={e => setEmail(e.target.value)} // the e.target.value property allows us to gain access to the input field's current value to be used when submitting form data
						// we use this to apply two way binding to break the default value of the state hook. Para makapagtype sa input fields
						required
					/> 
					<Form.Text className="text-muted">
						We'll naver share your email with anyone else!
					</Form.Text>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Enter your Password"
						value={password1} // if no onChange, binding ang value or locked sa ""
						onChange={e => setPassword1(e.target.value)} // This is called two-way binding
						required
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Verify Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Verify your Password"
						value={password2}
						onChange={e => setPassword2(e.target.value)}
						required
					/>
				</Form.Group>
				{isActive ?
				<Button variant="success" type="submit" id="submitBtn">Submit</Button>
						  :
				<Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
				}
			</Form>
		</Fragment>
	)
}