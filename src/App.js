  // Hooks don't work in class-based component. It needs redux
// Hooks - useState() stop, wait, go
      // - useEffect() waiting for state to render ( [] )
            // - function siya

//import React from 'react';

// Alternative
import { Fragment, useState, useEffect } from 'react';
// <React.fragment> to enclose components into a parent element para ma-avoid ang error
import './App.css';

// Dito magrender ng multiple components
import AppNavbar from './components/AppNavbar';
/*import Welcome from './components/Welcome';*/

// s45
import Home from './pages/Home';
import Courses from './pages/Courses';

// Dinala na sa home.js
/*import Banner from './components/Banner';
import Highlights from './components/Highlights';*/

// s46
/*import Counter from './components/Counter';*/
import Register from './pages/Register'
// Activity s46
import Login from './pages/Login';

// s47 Routing Components
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom'; // nakapaloob si Route sa Switch
import Error from './pages/Error';

/// Import React Context
import UserContext from './UserContext';
// Alternative if not working ang UserContext.Provider sa App.js
  // Destructuring
// import { UserProvider } from './UserContext';

// s48 
import Logout from './pages/Logout';

// s50
import SpecificCourse from './pages/SpecificCourse';

// Bootstrap
import { Container } from 'react-bootstrap'; 

/*
  React Context is a global state to the app. It is a way to make a particular data available to all the components no matter how they are nested. Context helps you to broadcast the data and changes happening to the data to all components
*/
function App() {          // Always dapat na may function
  
  // useState () for login purposes
  // Add a state hook for user
  // The getItem() method returns value of the specified Storage Object item
  const [user, setUser] = useState({ 
                    accessToken: localStorage.getItem('accessToken'), 
                    email: localStorage.getItem('email'),
                    isAdmin: localStorage.getItem('isAdmin') === 'true' // kailangan ito para kapag nagrefresh, hindi mag-iba ang admin status
                })
  // if walang localStorage, kapag nirefresh mawawala ang email. babalik sa login register, instead na logout

  // S48 Routing
  // Function for clearing localStorage on logout
  const unsetUser = () =>{
      localStorage.clear()
  }

  // Used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout
    // this is optional
  useEffect(() =>{
    console.log(user);
    console.log(localStorage)
  }, [user])

  return (                // It usually returns jsx
    <UserContext.Provider value={ {user, setUser, unsetUser} }> {/*All of the components and pages has access to this value*/}
  {/*Alternative
    <UserProvider> */}
        {/*Provider Component allows consuming components to subscribe to context changes (ReactContext
        Allows to send the changes. Para makapagpasa ng data*/}
      <Router>  {/*Pwede ng remove ang <Fragment> kapag may <UserContext.Provider>*/}
        <AppNavbar />
        <Container>
          <Switch> 
            {/*< Welcome />*/}
            < Route exact path="/"component={Home} /> {/*path property is only partial match. Hindi makukuha yung extension URL eg courses/react. courses/ lang ang makuha*/}
            < Route exact path="/courses"component={Courses} /> {/*exact path means specified url / route*/}
            < Route exact path="/courses/:courseId" component={SpecificCourse}/>
          {/*/:courseId is a wildcard*/}
            < Route exact path="/register"component={Register} /> 
            < Route exact path="/login"component={Login} />
            < Route exact path="/logout"component={Logout} />
            < Route component={Error}/> {/*Page not found basta wala sa exact path*/}
            {/*< Counter />*/}
          </Switch>
        </Container>
      </Router>
    </UserContext.Provider> // para maiset ang lahat ng State hooks globally para makapagpasa ng data
  );
}

export default App;       // To export the file in the function

/*
Router (Single Page Application)
  BrowserRouter component will enable us to simulate pagenavigation by synchronizing  the shown content and the shown URL in the web browser

  Switch component then declares with Route we can go to

  Route component will render components within the switch container based on the defined route

  exact property disables the 'partial matching' for a route and makes sure that it only returns the route if the path is an exact match to the current URL

  If exact and path is missing, the Route component will make it undefined route and will be loaded into a specified component

*/

/*

<div className="App">
      <h1>Hello World</h1>
</div>

Notes:

With the React fragment, we can group multiple components and avoid adding extra code

<Fragment> is preferred over <></> (shortcut) because it is not universal and can cause
problems in some other editors

JSX Syntax
JSX or JavaScript XML is an extension to the syntax of JS. It allows us to write 
HTML-like syntax written within our React js projects and it includes JS features as well.

  jsx - extension na pwede basahin ang JavaScript syntaxes
  Need to install JS(Babel) to run jsx for code readability
    1 Ctrl + Shift + P
    2. In the input field, type the wprd "install" and select the "package control: install package"
       option to trigger an installation of an add-on feature
    3. Type "babel" in the input field to be installed


>>Life cycle of components in react js<<
1. Mounting = overall front-end
2. Update = using states eg. when using an onclick event
3. Unmounting

Mounting > Rendering > Re-rendering > Unmounting

Example:
Login Page (Mounting) > Typing Data in Inputs (Rendering) Data Validation, Effect Hooks (Re- rendering) > Home (unmounting - changes component)

Hooks is the alternative to Redux
*/

