// Camel case dahil data lang

const courseData = [

	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.",
		price: 55000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc004",
		name: "JavaScript",
		description: "In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.",
		price: 40000,
		onOffer: true
	}

]

export default courseData;