
// This is where we save ALL of the State hooks
// We use separation of concerns instead of declaring a variable in App.js

import React from 'react';

export default React.createContext(); // property to create react context
									  // container where dito nakalagay lahat ng mga States
									  	// once a user logs in, magkakalaman na ito dahil sa set state

// The purpose of this is to pass down props to any of the children
	// A global state

// Alternative if not working ang UserContext.Provider sa App.js
/*import React from 'react';

const UserContext = React.createContext();
export const UserProvider = UserContext.Provider;

export default UserContext;*/